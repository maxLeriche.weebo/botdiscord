﻿using System;
using System.Collections.Generic;
using System.Text;
using MySqlConnector;
using MySql;
using MySql.Data.MySqlClient;
using System.Threading.Tasks;
using Discord;
using Discord.WebSocket;
using Discord.API;
using Discord.Commands;
using Discord.Audio;
using Discord.Net;


namespace DISCORD
{
    class bdd
    {
        public  MySqlConnection connection;
        private string host = "weebo.fr";
        private string port = "3306";
        private string id = "projview";
        private string mdp = "RTFxoeKWbOycXbXf";
        private string database = "M3104";
        private string[][] retour=new string[54][];
        private List<string[]> hw = new List<string[]>();

        private List<string[]> Commande = new List<string[]>();

        public string[][] Retour { get => retour; }
        
        public List<string[]> getHW() { return hw; }
        public List<string[]> getCommande() { return Commande; }

        public bool connect()
        {
            try
            {
                string acces = "host=" + host + ";port=" + port + ";user id=" +id + ";password=" +mdp+ ";database=" + database;
                connection = new MySqlConnection(acces);
            }
            catch
            {
                return false;
            }
            return true;
        }
        public void clean()
        {
            
            hw.Clear();
        }

        public async Task GetHw()
        {
            clean();
            try
            {

                connection.Close();
                connect();

                await connection.OpenAsync();
                using (var cmd = new MySqlCommand("select * from Devoir", connection))
                await using (var reader = await cmd.ExecuteReaderAsync())
                while (await reader.ReadAsync())
                {
                        string[] homework = new string[10];
                        homework[0] = reader.GetValue(0).ToString();
                        homework[1] = reader.GetString(1).ToString();
                        homework[2] = reader.GetString(2).ToString();
                        homework[3] = reader.GetString(3).ToString();
                        homework[4] = reader.GetString(4).ToString();
                        homework[5] = reader.GetString(5).ToString();
                        homework[6] = reader.GetString(6).ToString();
                        homework[7] = reader.GetString(7).ToString();
                        homework[8] = reader.GetString(8).ToString();
                        homework[9] = reader.GetString(9).ToString();
                        hw.Add(homework);
                    }
            }
            catch (Exception ee)
            {
                throw ee;
            }

        }

        public async Task GetHw(string groupe)
        {
            clean();
            try
            {

                connection.Close();
                connect();

                await connection.OpenAsync();
                using (var cmd = new MySqlCommand("select * from Devoir where groupe = @p", connection))
                {
                    cmd.Parameters.AddWithValue("p", groupe);
                    await using (var reader = await cmd.ExecuteReaderAsync())
                    {
                        while (await reader.ReadAsync())
                        {
                            string[] homework = new string[10];
                            homework[0] = reader.GetValue(0).ToString();
                            homework[1] = reader.GetString(1);
                            homework[2] = reader.GetString(2);
                            homework[3] = reader.GetString(3);
                            homework[4] = reader.GetString(4);
                            homework[5] = reader.GetString(5);
                            homework[6] = reader.GetString(6);
                            homework[7] = reader.GetString(7);
                            homework[8] = reader.GetString(8);
                            homework[9] = reader.GetString(9);
                            hw.Add(homework);
                        }
                    }
                }
                
                    
            }
            catch(Exception ee)
            {
                throw ee;
            }

        }

        public async Task SetHw(string pseudo,string matiere,string Date,string PourQuand,string Description,string prof,string engroupe,string diff,string group)
        {//praise hw add groupe engroupe prof matiere pourquand diff description
            try
            {
                connection.Close();
                connect();
                
                await connection.OpenAsync();
                using (var cmd = new MySqlCommand())
                {
                    cmd.Connection = connection;
                    
                    cmd.CommandText = "insert into Devoir (quiLaCreer,matiere,dateparution,PourQuand,Description,prof,Engroupe,Difficulte,groupe) Values (@qui, @m, @dis,@quand, @descri,@prof,@eg,@diff,@group)";
                    cmd.Parameters.AddWithValue("qui", pseudo);
                    cmd.Parameters.AddWithValue("m",matiere);
                    cmd.Parameters.AddWithValue("dis", Date);
                    cmd.Parameters.AddWithValue("quand", PourQuand);
                    cmd.Parameters.AddWithValue("descri", Description);
                    cmd.Parameters.AddWithValue("prof", prof);
                    cmd.Parameters.AddWithValue("eg",engroupe );
                    cmd.Parameters.AddWithValue("diff",diff);
                    cmd.Parameters.AddWithValue("group",group);
                    await cmd.ExecuteNonQueryAsync();
                    return;
                }  
            }
            catch(Exception ee)
            {
                Console.WriteLine(ee.Message);
            }
        }

        public async Task getComm() //BOT_COMMANDE
        {
            Commande.Clear();
            try
            {

                connection.Close();
                connect();

                await connection.OpenAsync();
                using (var cmd = new MySqlCommand("select * from BOT_COMMANDE", connection))
                await using (var reader = await cmd.ExecuteReaderAsync())
                    while (await reader.ReadAsync())
                    {
                        string[] comma = new string[3];
                        comma[0] = reader.GetValue(0).ToString();
                        comma[1] = reader.GetString(1).ToString();
                        comma[2] = reader.GetString(2).ToString();
                        Commande.Add(comma);
                    }
            }
            catch (Exception ee)
            {
                throw ee;
            }
        }

        public async Task getBotKeyword()
        {
            try
            {
                await connection.OpenAsync();
                
            }
            catch
            {
                Exception test = new Exception("Erreur lors de l'ouverture de la Connection avec la bdd");
                throw test;
            }
            try
            {
                for(int iteration = 0; iteration < 54; iteration++) { retour[iteration] = new string[2]; }
                int boucle = 0;
                Console.WriteLine("step one");
                using (var cmd = new MySqlCommand("select * from bot", connection))
                await using (var reader = await cmd.ExecuteReaderAsync())
                    while (await reader.ReadAsync())
                    {
                        Console.WriteLine("step two");
                        retour[boucle][0] = reader.GetString(0);
                        retour[boucle][1] = reader.GetString(1);
                        boucle += 1;
                        Console.WriteLine("step tree");
                    }
                for(int iteration =0;iteration<retour[0].Length;iteration++)
                {
                    Console.WriteLine(retour[iteration][0] + " : " + retour[iteration][1]);
                }
            }
            catch
            {
                Exception test = new Exception("erreur lors de l'affectation sur le tab");
            }
            return;
        }


        public void disconnect()
        {
            connection.Close();
        }
    }
}
