﻿using System;
using System.Threading.Tasks;
using Discord;
using Discord.WebSocket;
using Discord.API;
using Discord.Commands;
using Discord.Audio;
using Discord.Net;

namespace DISCORD
{
    class Program
    {
        private readonly DiscordSocketClient _client;
        private bool keyword = true;
        private DateTime DATE = new DateTime();
        private string pathtopicture = "../../../../banque_image";
        string[] directory;
        bdd BaseDonne = new bdd();
        string[] file;
        private string[][] keywordlist;
        Random rnd = new Random(2);
        int randomprevious = 0;

        // Discord.Net heavily utilizes TAP for async, so we create
        // an asynchronous context from the beginning.
        static void Main(string[] args)
        {
            new Program().MainAsync().GetAwaiter().GetResult();
        }
        
        public Program()
        {
            // It is recommended to Dispose of a client when you are finished
            // using it, at the end of your app's lifetime.
            _client = new DiscordSocketClient();
            _client.Log += LogAsync;
            _client.Ready += ReadyAsync;
            _client.MessageReceived += MessageReceivedAsync;
            //_client.SetGameAsync("Regarde Iron Blooded Orphan");
            _client.SetStatusAsync(UserStatus.Idle);
            _client.SetGameAsync("Iron Blooded Orphan","" , ActivityType.Watching);
            openfolder();
            BaseDonne.connect();
            BaseDonne.getBotKeyword();
            keywordlist = BaseDonne.Retour;
            Console.Clear();
            Console.WriteLine(@" __      __       .__          ._.
/  \    /  \ ____ |  |   ____  ____   _____   ____| |
\   \/\/   // __ \|  | _/ ___\/  _ \ /     \_/ __ \ |
 \        /\  ___/|  |_\  \__(  <_> )  Y Y  \  ___/\|
  \__/\  /  \___  >____/\___  >____/|__|_|  /\___  >_
       \/       \/          \/            \/     \/\/");
            
            
        }

        public async void hw(string[] content,SocketMessage msg)
        {
            if(content.Length >=10&&content[2].ToLower()=="add")
            {//praise hw add groupe engroupe prof matiere pourquand diff description
                string[] cut = msg.Content.Split(content[8]); //(string pseudo,string matiere,string Date,string PourQuand,string Description,string prof,string engroupe,string diff,string group)
                await BaseDonne.SetHw(msg.Author.Username.ToString(), content[6].ToLower(), DateTime.Now.ToString(), content[7].ToLower(), cut[1].ToLower(), content[5].ToLower(), content[6].ToLower(), content[8].ToLower(), content[3].ToLower());
                return;
            }
            //await BaseDonne.GetHw();
            if(content.Length>=3)
            if(content[2].ToLower()=="show")
            {
                    Console.WriteLine("enter dans show");
                if (content.Length>3&&Int32.TryParse(content[3], out int test))
                {
                    if (test > 0 && test < 6)
                    {
                        await BaseDonne.GetHw(test.ToString());
                            if (BaseDonne.getHW().Count == 0)
                            {
                                await msg.Channel.SendMessageAsync("Pas de devoir stocker");
                                return;
                            }
                            for (int iteration2 = 0; iteration2 < BaseDonne.getHW().Count; iteration2++)
                            {
                                EmbedBuilder builder = new EmbedBuilder();

                                builder.WithTitle("Devoir numéro " + BaseDonne.getHW()[iteration2][0]);
                                builder.AddField("matiére", BaseDonne.getHW()[iteration2][2], true);    // true - for inline
                                builder.AddField("Dificulté", BaseDonne.getHW()[iteration2][8], true);
                                builder.AddField("groupe", BaseDonne.getHW()[iteration2][9], true);
                                builder.AddField("En groupe:", BaseDonne.getHW()[iteration2][7], true);
                                builder.AddField("PRoF", BaseDonne.getHW()[iteration2][6], true);
                                builder.AddField("Pour Quand?", BaseDonne.getHW()[iteration2][4], true);
                                builder.AddField("Description", BaseDonne.getHW()[iteration2][5], false);
                                builder.WithColor(Color.Red);
                                builder.WithTimestamp(DateTimeOffset.Now);
                                await msg.Channel.SendMessageAsync("Enjoy your homework", false, builder.Build());
                            }//BaseDonne.getHW()[iteration2][]
                    }
                    else
                    {
                        await msg.Channel.SendMessageAsync("Erreur le numéro:\n Ta oublier ton numéro de groupe ?");
                    }
                    return;
                }
                else
                {
                    await BaseDonne.GetHw();
                    if (BaseDonne.getHW().Count == 0) {
                            await msg.Channel.SendMessageAsync("Pas de devoir stocker");
                            return; }
                    for (int iteration2 = 0; iteration2 < BaseDonne.getHW().Count; iteration2++)
                    {
                        EmbedBuilder builder = new EmbedBuilder();

                        builder.WithTitle("Devoir numéro " + BaseDonne.getHW()[iteration2][0]);
                        builder.AddField("matiére", BaseDonne.getHW()[iteration2][2], true);    // true - for inline
                        builder.AddField("Dificulté", BaseDonne.getHW()[iteration2][8], true);
                        builder.AddField("groupe", BaseDonne.getHW()[iteration2][9], true);
                        builder.AddField("En groupe:", BaseDonne.getHW()[iteration2][7], true);
                        builder.AddField("PRoF", BaseDonne.getHW()[iteration2][6], true);
                        builder.AddField("Pour Quand?", BaseDonne.getHW()[iteration2][4], true);
                        builder.AddField("Description", BaseDonne.getHW()[iteration2][5], false);
                        builder.WithColor(Color.Red);
                        builder.WithTimestamp(DateTimeOffset.Now);
                        await msg.Channel.SendMessageAsync("Enjoy your homework", false, builder.Build());
                    } //BaseDonne.getHW()[iteration2][]
                    
                    return;
                }
            }
            await msg.Channel.SendMessageAsync("Ouais mais non tu t'est un peu foirer et tu a oublier un argument... ou pas xD, Fait appelle a la doc.... Oh wait.... elle ne marche pas encore.... Sorry");
        }

        public async void help(string[] content,SocketMessage msg)
        {
            await BaseDonne.getComm();
            if(BaseDonne.getCommande().Count>0)
            {
                EmbedBuilder builder = new EmbedBuilder();
                builder.WithTitle("Liste de Commande");
                builder.AddField("Commande", BaseDonne.getCommande()[0][0], true);    // true - for inline
                builder.AddField("Description", BaseDonne.getCommande()[0][1], true);
                builder.AddField("Usage", BaseDonne.getCommande()[0][2], false);
                builder.WithColor(Color.Red);
                builder.WithTimestamp(DateTimeOffset.Now);
                await msg.Channel.SendMessageAsync(" ", false, builder.Build());
                for (int iterationx =1; iterationx<BaseDonne.getCommande().Count;iterationx++)
                {
                    builder = new EmbedBuilder();
                    builder.AddField("Commande", BaseDonne.getCommande()[iterationx][0], true);    // true - for inline
                    builder.AddField("Description", BaseDonne.getCommande()[iterationx][1], true);
                    builder.AddField("Usage", BaseDonne.getCommande()[iterationx][2], false);
                    builder.WithColor(Color.Red);
                    await msg.Channel.SendMessageAsync(" ", false, builder.Build());
                }

                
            }
            else
            {
                await msg.Channel.SendMessageAsync("Ouais mais j'ai fait une bourde avec la bdd de commande je crois");
            }
            return;
        }

        public async Task MainAsync()
        {
            // Tokens should be considered secret data, and never hard-coded.
            await _client.LoginAsync(TokenType.Bot, "NTE2MzUzMzAzMjE5Nzk4MDIx.XZ8Syw.Og8_DqjptaOOg8JGGplloTIGJhw");
            await _client.StartAsync();

            // Block the program until it is closed.
            await Task.Delay(-1);
        }

        private Task LogAsync(LogMessage log)
        {
            Console.WriteLine(log.ToString());  
            return Task.CompletedTask;
        }

        // The Ready event indicates that the client has opened a
        // connection and it is now safe to access the cache.
        private Task ReadyAsync()
        {
            Console.WriteLine($"{_client.CurrentUser} is connected!");

            return Task.CompletedTask;
        }
        private Task openfolder()
        {
            directory = System.IO.Directory.GetDirectories(pathtopicture);
            return Task.CompletedTask;
        }
        private Task listfile(string contenant)
        {

            file = System.IO.Directory.GetFiles((pathtopicture + "/" + contenant));
            Console.WriteLine(file.Length);
            return Task.CompletedTask;
        }

        // This is not the recommended way to write a bot - consider
        // reading over the Commands Framework sample.
        public int nass = 0;
        private async Task MessageReceivedAsync(SocketMessage message)
        {
            if(message.Author.Username.Contains("nasslamenace")&&nass==1)
            {
                Console.WriteLine("ta gueule nass");
                var msg = await message.Channel.GetMessagesAsync(1).FlattenAsync(); //defualt is 100
                Console.WriteLine(message.Content);
                await (message.Channel as SocketTextChannel).DeleteMessagesAsync(msg);
                return;
            }
            if ((message.Content.ToLower().Contains("nano")||message.Content.ToLower().Contains("n a n o")||(message.Content.ToLower().Contains("na") && message.Content.ToLower().Contains("no"))) && message.Author.Username.ToLower().Contains("nasslamenace"))
            {
                string cont = message.Content.ToLower().Split(' ')[1];
                    var msg = await message.Channel.GetMessagesAsync(1).FlattenAsync(); //defualt is 100
                    await (message.Channel as SocketTextChannel).DeleteMessagesAsync(msg);
               await message.Channel.SendMessageAsync("Nano is bae");
                return;
                
            }
            // The bot should never respond to itself.
            if (message.Author.Id == _client.CurrentUser.Id)
                return;
            if (message.Content.ToLower().Contains("!activity") && message.Author.Username.Contains("batleforc"))
            {
                if (message.Content.Split(" ").Length > 1 && message.Content.Split(" ")[1].ToLower().Contains("1"))
                {
                   await _client.SetGameAsync(message.Content.Split(message.Content.Split(" ")[1])[1], "", ActivityType.Playing);
                }
                else
                {
                   await _client.SetGameAsync("Iron Blooded Orphan", "", ActivityType.Watching);
                }

            }
            string[] content = message.Content.Split(" ");
            if (content[0].ToLower() == "praise")
            {
                if (content[1] == "ping")
                {
                    await message.Channel.SendMessageAsync("pong!");
                }
                if (content[1]== "nass" && message.Author.Username.Contains("batleforc"))
                {
                    if (nass == 1)
                    {
                        nass = 0;
                        Console.WriteLine("j'ai pitier de toi");
                    }
                    else
                    {
                        nass = 1;
                        Console.WriteLine("Ta gueule nass");
                    }
                    return;
                }
                if(content[1].ToLower().Contains('@'))
                {
                    await message.Channel.SendMessageAsync("ROHO! YOU'RE APPROCHING ME!? " + content[1] + " , KISAMA DA!");
                }
                if(content[1].ToLower() == "nsfw")
                {
                    SocketTextChannel test1 = (SocketTextChannel)message.Channel;
                    

                    if (test1.IsNsfw)
                    {
                        await message.Channel.SendMessageAsync("NSFWWWW");
                    }
                    else
                    {
                        await message.Channel.SendMessageAsync("Euh tu a penser aux esprit pure ? balancer du nsfw dans un channel accessible par tous en plus !!");
                    }
                }
                if(content[1].ToLower()=="help")
                {
                    help(content, message);
                    return;
                }
                if(content[1].ToLower() == "keyword")
                {
                    if(content.Length>=3&&content[2]=="update"&&message.Author.Username.Contains("batleforc"))
                    {
                        await openfolder();
                        await message.Channel.SendMessageAsync("dossier de la banque d'image "+ directory.Length);
                        for (int i = 0; i<directory.Length;i++)
                        {
                            await message.Channel.SendMessageAsync(i+" "+directory[i]);
                        }
                        await message.Channel.SendMessageAsync("fin de la liste");
                        return;
                    }
                    if (content.Length >= 3 && content[2] == "db" && message.Author.Username.Contains("batleforc"))
                    {
                        BaseDonne.connect();
                        await BaseDonne.getBotKeyword();
                        Console.WriteLine("db");
                        keywordlist = BaseDonne.Retour;
                        return;
                    }
                    if (content.Length >= 3 && content[2] == "disconnect" && message.Author.Username.Contains("batleforc"))
                    {
                        BaseDonne.disconnect();
                        Console.WriteLine("Disconnect");
                        return;
                    }
                    if (keyword)
                    {
                        keyword = false;
                    }
                    else
                    {
                        keyword = true;
                    }
                    await message.Channel.SendMessageAsync("Key word :" + keyword);
                    return;
                }
                if(content[1].ToLower() == "edt")
                {
                    if(content.Length==2)
                    {
                        await message.Channel.SendMessageAsync("Veuillez préciser le groupe");
                        return;
                    }
                    
                    int num_semaine = System.Globalization.CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(DateTime.Now, System.Globalization.CalendarWeekRule.FirstFullWeek, DayOfWeek.Monday);
                    int IDGROUPE=0;int jour = 0;
                    if(content.Length==4)
                    {
                        Console.WriteLine("Entrer dans edt par jour:");
                       if( !Int32.TryParse(content[3], out jour))
                        {
                            Console.WriteLine("Entrer dans edt par jour step 2:");
                            Console.WriteLine(DateTime.Today.DayOfWeek.ToString().ToLower());
                            switch (DateTime.Today.DayOfWeek.ToString().ToLower())
                            {
                                case "monday":
                                    switch (content[3].ToLower())
                                    {
                                        case "lundi":
                                            jour = 0;
                                            break;
                                        case "mardi":
                                            jour = 1;
                                            break;
                                        case "mercredi":
                                            jour = 2;
                                            break;
                                        case "jeudi":
                                            jour = 3;
                                            break;
                                        case "vendredi":
                                            jour = 4;
                                            break;
                                        case "samedi":
                                            jour = 5;
                                            break;
                                        default:
                                            await message.Channel.SendMessageAsync("Tu sait comment écrire un jour en francais ?");
                                            return;
                                    }
                                    break;
                                case "tuesday":
                                    switch (content[3].ToLower())
                                    {
                                        
                                        case "lundi":
                                            jour = -1;
                                            break;
                                        case "mardi":
                                            jour = 0;
                                            break;
                                        case "mercredi":
                                            jour = 1;
                                            break;
                                        case "jeudi":
                                            jour = 2;
                                            break;
                                        case "vendredi":
                                            jour = 3;
                                            break;
                                        case "samedi":
                                            jour = 4;
                                            break;
                                        default:
                                            await message.Channel.SendMessageAsync("Tu sait comment écrire un jour en francais ?");
                                            return;
                                    }
                                    break;
                                case "wednesday":
                                    switch (content[3].ToLower())
                                    {
                                        case "lundi":
                                            jour = -2;
                                            break;
                                        case "mardi":
                                            jour = -1;
                                            break;
                                        case "mercredi":
                                            jour = 0;
                                            break;
                                        case "jeudi":
                                            jour = 1;
                                            break;
                                        case "vendredi":
                                            jour = 2;
                                            break;
                                        case "samedi":
                                            jour = 3;
                                            break;
                                        default:
                                            await message.Channel.SendMessageAsync("Tu sait comment écrire un jour en francais ?");
                                            return;
                                    }
                                    break;
                                case "thursday":
                                    switch (content[3].ToLower())
                                    {
                                        case "lundi":
                                            jour = -3;
                                            break;
                                        case "mardi":
                                            jour = -2;
                                            break;
                                        case "mercredi":
                                            jour = -1;
                                            break;
                                        case "jeudi":
                                            jour = 0;
                                            break;
                                        case "vendredi":
                                            jour = 1;
                                            break;
                                        case "samedi":
                                            jour = 2;
                                            break;
                                        default:
                                            await message.Channel.SendMessageAsync("Tu sait comment écrire un jour en francais ?");
                                            return;
                                    }
                                    break;
                                case "friday":
                                    switch (content[3].ToLower())
                                    {
                                        case "lundi":
                                            jour = -4;
                                            break;
                                        case "mardi":
                                            jour = -3;
                                            break;
                                        case "mercredi":
                                            jour = -2;
                                            break;
                                        case "jeudi":
                                            jour = -1;
                                            break;
                                        case "vendredi":
                                            jour = 0;
                                            break;
                                        case "samedi":
                                            jour = 1;
                                            break;
                                        default:
                                            await message.Channel.SendMessageAsync("Tu sait comment écrire un jour en francais ?");
                                            return;
                                    }
                                    break;
                                case "saturday":
                                    switch (content[3].ToLower())
                                    {
                                        case "lundi":
                                            jour = -5;
                                            break;
                                        case "mardi":
                                            jour = -4;
                                            break;
                                        case "mercredi":
                                            jour = -3;
                                            break;
                                        case "jeudi":
                                            jour = -2;
                                            break;
                                        case "vendredi":
                                            jour = -1;
                                            break;
                                        case "samedi":
                                            jour = 0;
                                            break;
                                        default:
                                            await message.Channel.SendMessageAsync("Tu sait comment écrire un jour en francais ?");
                                            return;
                                    }
                                    break;
                                case "sunday":
                                    switch (content[3].ToLower())
                                    {
                                        case "lundi":
                                            jour = 1;
                                            break;
                                        case "mardi":
                                            jour = 2;
                                            break;
                                        case "mercredi":
                                            jour = 3;
                                            break;
                                        case "jeudi":
                                            jour = 4;
                                            break;
                                        case "vendredi":
                                            jour = 5;
                                            break;
                                        case "samedi":
                                            jour = 6;
                                            break;
                                        default:
                                            await message.Channel.SendMessageAsync("Tu sait comment écrire un jour en francais ?");
                                            return;
                                    }
                                    break;
                            }

                        }
                       
                    }
                    else
                    {
                        jour = 0;
                    }
                    
                    switch(content[2].ToLower())
                    {
                        case "g1":
                            IDGROUPE = 574;
                            break;
                        case "g2":
                            IDGROUPE = 575;
                            break;
                        case "g3":
                            IDGROUPE = 576;
                            break;
                        case "g4":
                            IDGROUPE = 577;
                            break;
                        case "g5":
                            IDGROUPE = 578;
                            break;
                        default:
                            await message.Channel.SendMessageAsync("Euh tes aux courant qu'il n'existe que 5 groupe de 1 a 5 ?");
                            return;
                            
                    }
                    string url = "https://edt.iut-amiens.fr/vue_etudiant_journaliere.php?current_year=" + DATE.Year + "&current_student=" + IDGROUPE + "&current_week=" + num_semaine + "&lar=1408&hau=" + (660 + DATE.DayOfWeek) +"&jour=" +jour;
                    await message.Channel.SendMessageAsync("EDT dut groupe " + content[2]);
                    await message.Channel.SendMessageAsync(url);
                }
                if(content[1].ToLower() == "spam"&&content.Length==5 && message.Author.Username.Contains("batleforc"))
                {
                    Console.WriteLine("entrer dans troll");
                    Int32.TryParse(content[2], out int test);
                    for (int iteration =0;iteration<test;iteration++)
                    {
                        Console.WriteLine("iteration:" + iteration);
                        await message.Channel.SendMessageAsync(content[3] + " " + content[4]);
                    }
                    return;
                }
                if(content[1].ToLower()=="javanais")
                {
                    string retour=" ";
                    for(int iteration=2;iteration<content.Length;iteration++)
                    {//a e y u i o
                        if (content[iteration].Contains('a'))
                        {
                            string[] swap = content[iteration].Split('a');
                            retour = retour + swap[0];
                            for (int iteration2 =1;iteration2<swap.Length;iteration2++)
                            {
                                retour =retour +"ava"+swap[iteration2];
                            }
                        }
                        if (content[iteration].Contains('e'))
                        {
                            string[] swap = content[iteration].Split('e');
                            retour = retour + swap[0];
                            for (int iteration2 = 0; iteration2 < swap.Length; iteration2++)
                            {
                                retour = retour + "ave" + swap[iteration2];
                            }
                        }
                        if (content[iteration].Contains('y'))
                        {
                            string[] swap = content[iteration].Split('y');
                            retour = retour + swap[0];
                            for (int iteration2 = 0; iteration2 < swap.Length; iteration2++)
                            {
                                retour = retour + "avy" + swap[iteration2];
                            }
                        }
                        if (content[iteration].Contains('u'))
                        {
                            string[] swap = content[iteration].Split('u');
                            retour = retour + swap[0];
                            for (int iteration2 = 0; iteration2 < swap.Length; iteration2++)
                            {
                                retour = retour +  "avu" + swap[iteration2 + 1];
                            }
                        }
                        if (content[iteration].Contains('i'))
                        {
                            string[] swap = content[iteration].Split('i');
                            retour = retour + swap[0];
                            for (int iteration2 = 0; iteration2 < swap.Length ; iteration2++)
                            {
                                retour = retour +"avi" + swap[iteration2 + 1];
                            }
                        }
                        if (content[iteration].Contains('o'))
                        {
                            string[] swap = content[iteration].Split('o');
                            retour = retour + swap[0];
                            for (int iteration2 = 0; iteration2 < swap.Length ; iteration2++)
                            {
                                retour = retour +"avo" + swap[iteration2 + 1];
                            }
                        }
                        //message.Channel.SendMessageAsync("traitement " + (iteration-1) + "mots");
                        retour = retour + " ";
                    }
                    await message.Channel.SendMessageAsync(retour);
                    return;

                }
                if(content[1].ToLower()=="idchan" && message.Author.Username.Contains("batleforc"))
                {
                   await message.Author.SendMessageAsync(message.Channel.Id.ToString());
                    
                }
                if(content[1].ToLower() == "rpa" && message.Author.Username.Contains("batleforc"))
                {
                    ulong general = 573430286042398733;
                    var channel = _client.GetChannel(general) as IMessageChannel;
                    string[] temporaire = message.Content.Split(content[1]);
                    await channel.SendMessageAsync(temporaire[1]);
                }
                if(content[1].ToLower()=="sandbox"&& message.Author.Username.Contains("batleforc"))
                {
                    EmbedBuilder builder = new EmbedBuilder();

                    builder.WithTitle("Devoir numéro (temporaire)");
                    builder.AddField("matiére", "ANGLAIS", true);    // true - for inline
                    builder.AddField("Dificulté", "665", true);
                    builder.AddField("groupe", "42", true);
                    builder.AddField("En groupe:", "False", true);
                    builder.AddField("PRoF", "CleCle la waifu", true);
                    builder.AddField("Pour Quand?"," demain demain",true);
                    builder.AddField("Créateur", "MOUAHAHAHAHAH", true);
                    builder.AddField("Description", "35%", false);
                    builder.WithColor(Color.Red);
                    builder.WithTimestamp(DateTimeOffset.Now);
                    await message.Channel.SendMessageAsync("Enjoy your homework", false, builder.Build());
                    return;
                }
                if (content[1].ToLower() == "hw")
                {
                    hw(content,message);
                    return;
                }
            }
            else if (content[0][0] == 'b' && content[0][1] == '.')
            {
                return;
            }
            else if (keyword&&!message.Author.IsBot)
            {
                
                foreach(string[] swap in keywordlist)
                {
                    
                    if(message.Content.ToLower().Contains(swap[0]))
                    {
                        bool test1 = false;
                        bool test2 = false;
                        bool test3 = false;
                        if((message.Content.ToLower().Length == swap[0].Length))
                        {
                            test1 = true;
                        }
                        else if (((message.Content.ToLower().Split(swap[0]).Length > 1) && message.Content.ToLower().Split(swap[0])[1][0] == ' '))
                        {
                            test2 = true;
                        }
                        else if((message.Content.ToLower().Split(swap[0]).Length == 1))
                        {
                            test3 = true;
                        }
                        if(test1||test2||test3)
                        {
                            Console.WriteLine("entrer dans la deuxiéme condition keywordlist");
                            string imagechoisie;
                            await listfile(swap[1]);
                            int temporaire = 100;
                            if (file.Length > 1)
                            {
                                do
                                {
                                    temporaire = rnd.Next(file.Length);
                                    Console.WriteLine(temporaire + " : " + randomprevious);
                                } while (temporaire == randomprevious);
                                imagechoisie = file[temporaire];
                                randomprevious = temporaire;
                            }
                            else
                            {
                                imagechoisie = file[rnd.Next(file.Length)];
                            }
                            await message.Channel.SendFileAsync(imagechoisie);
                            return;
                        }
                        
                    }
                }

                if (message.Content.Contains("fuite") || message.Content.Contains("FUITE"))
                {
                    await message.Channel.SendMessageAsync("https://tenor.com/view/nigerundayo-jojo-corramos-run_away-huyamos-gif-10932968");
                }

            }
            

            
        }
    }
}

